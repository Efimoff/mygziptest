﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace MyGZipTest
{
    class DeCompresingFile
    {
        private int bufferLength = 0;
        private string sourceFile;
        private string destinationFile;
        private bool flagDeComresion;
        private readonly int arrayBlocks = 5;
        private long lengthLastBlock = 0;

        private Queue<Structures.Block> queueRead = new Queue<Structures.Block>();
        private Queue<Structures.Block> queueDeCompression = new Queue<Structures.Block>();
        private Structures.Option option;

        private AutoResetEvent _event;
        private AutoResetEvent _event1;

        private long countBlocks = 0;

        public DeCompresingFile(string sourceF, string destinationF)
        {
            sourceFile = sourceF;
            destinationFile = destinationF;

            flagDeComresion = true;

            _event = new AutoResetEvent(false);
            _event1 = new AutoResetEvent(false);
        }

        public void Work()
        {
            int processorCount = Environment.ProcessorCount;
            Thread read = new Thread(() =>
            {
                ReadBlock();
            });

            Thread write = new Thread(() =>
            {
                WriteDeCompressBlock();
            });
            for (int i = 0; i < processorCount * 2; i++)
            {
                Thread deComp = new Thread(() =>
                {
                    BlockDeCompression();
                });
                deComp.Start();
            }

            read.Start();
            write.Start();
        }

        public void ReadBlock()
        {
            using (FileStream sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
            using (BinaryReader br = new BinaryReader(sourceStream))
            {
                int i = 0;
                GetOptions(br);
                while (sourceStream.CanRead)
                {
                    if (queueRead.Count >= arrayBlocks)
                    {
                        _event1.WaitOne();
                    }
                    var block = ReadNewBlock(br);
                    lock (queueRead)
                    {
                        queueRead.Enqueue(block);
                        i++;
                        if (i == countBlocks)
                        {
                            break;
                        }
                    }
                }
            }
            flagDeComresion = false;
        }

        private void GetOptions(BinaryReader source)
        {
            option.fileLength = source.ReadInt64();
            option.block = source.ReadInt32();
            bufferLength = option.block;

            countBlocks = CalculateCountBlocks(option.fileLength, option.block);
            lengthLastBlock = option.fileLength - (option.block * (countBlocks - 1));
        }

        private long CalculateCountBlocks(long length, int block)
        {
            long count = 0;

            if (length % block != 0)
            {
                count = (length / block) + 1;
            }
            else
            {
                count = length / block;
            }

            return count;
        }

        private Structures.Block ReadNewBlock(BinaryReader source)
        {
            Structures.Block newBlock = new Structures.Block();

            try
            {
                newBlock.idBlock = source.ReadInt32();
                var bsize = source.ReadInt32();
                newBlock.block = source.ReadBytes(bsize);
                return newBlock;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("\nFile stopped reading.");

                newBlock.block = new byte[] { };
                return newBlock;
            }
        }

        private void BlockDeCompression()
        {
            Structures.Block block = new Structures.Block();

            while (flagDeComresion || queueRead.Count != 0)
            {
                block.idBlock = 0;

                lock (queueRead)
                {
                    if (queueDeCompression.Count <= arrayBlocks && queueRead.Count != 0)
                    {
                        block = queueRead.Dequeue();

                        _event1.Set();
                    }

                    if (queueRead.Count >= arrayBlocks)
                    {
                        _event.WaitOne();
                    }
                }

                if (block.idBlock != 0)
                {
                    var deBlock = DeCompression(block);

                    lock (queueDeCompression)
                    {
                        queueDeCompression.Enqueue(deBlock);
                    }
                }
            }
        }

        private Structures.Block DeCompression(Structures.Block block)
        {
            Structures.Block deComprBlock = new Structures.Block();

            using (var stream = new MemoryStream(block.block))
            {
                using (var gZipStream = new GZipStream(stream, CompressionMode.Decompress))
                {
                    byte[] decompressedBytes = new byte[bufferLength];
                    long bLength = bufferLength;
                    if (block.idBlock == countBlocks)
                    {
                        decompressedBytes = new byte[lengthLastBlock];
                        bLength = lengthLastBlock;
                    }
                    gZipStream.Read(decompressedBytes, 0, decompressedBytes.Length);

                    deComprBlock.block = decompressedBytes;
                    deComprBlock.idBlock = block.idBlock;
                }
            }
            return deComprBlock;
        }


        private void WriteDeCompressBlock()
        {
            int i = 0;
            using (var dest = new FileStream(destinationFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                while (dest.CanWrite)
                {
                    if (queueDeCompression.Count != 0)
                    {
                        var writeBlock = GetDeCompressBlock();

                        if (writeBlock.block.Length > 0)
                        {
                            WriteDeBlock(dest, writeBlock);
                            i++;
                        }
                    }
                    if (i >= countBlocks && !flagDeComresion)
                    {
                        dest.Close();
                    }
                }
            }
            if (i >= countBlocks)
            {
                Console.WriteLine("Decompression was successfully completed");
                Console.WriteLine("D - list of commands");
            }
        }

        private Structures.Block GetDeCompressBlock()
        {
            Structures.Block compressBlock = new Structures.Block();
            lock (queueDeCompression)
            {
                compressBlock = queueDeCompression.Dequeue();

                _event.Set();
            }
            return compressBlock;
        }

        private void WriteDeBlock(Stream write, Structures.Block block)
        {
            try
            {
                long idB = block.idBlock - 1;
                long buf = bufferLength;
                write.Seek((idB * bufferLength), SeekOrigin.Begin);
                write.Write(block.block, 0, block.block.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine(block.idBlock);
                Console.WriteLine(block.block.Length);
                Console.WriteLine(e);
                Console.WriteLine("\nFile write terminated.");
            }
        }
    }
}
