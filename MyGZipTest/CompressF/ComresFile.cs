﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.IO.Compression;

namespace MyGZipTest
{
    class ComresFile
    {
        private int bufferLength;
        private string sourceFile;
        private string destinationFile;
        bool flagReadBlocks;
        private byte[] streamBuffer;
        private readonly int arrayBlocks = 5;

        private Queue<Structures.Block> queueRead = new Queue<Structures.Block>();
        private Queue<Structures.Block> queueCompression = new Queue<Structures.Block>();
        private Structures.Option option;

        private AutoResetEvent _event;
        private AutoResetEvent _event1;

        private double countBlocks = 0;

        public ComresFile(int bufferL, string sourceF, string destinationF)
        {
            bufferLength = bufferL;
            sourceFile = sourceF;
            destinationFile = destinationF;
            streamBuffer = new byte[bufferLength];

            flagReadBlocks = true;

            _event = new AutoResetEvent(true);
            _event1 = new AutoResetEvent(true);
        }

        public void Work()
        {
            int processorCount = Environment.ProcessorCount;

            Thread add = new Thread(() =>
            {
                AddReadBlock();

            });
            add.Start();

            for (int i = 0; i < processorCount * 2; i++)
            {
                Thread comp = new Thread(() =>
                {
                    CompressBlock();
                });
                comp.Start();
            }

            Thread write = new Thread(() =>
            {
                WriteCompressBlock();
            });
            write.Start();
        }

        private void AddReadBlock()
        {
            using (var source = new FileStream(sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (BinaryReader br = new BinaryReader(source))
            {
                CreateOptions(source.Length);

                int numRead = 0;
                while (source.CanRead)
                {
                    if (queueRead.Count >= arrayBlocks)
                    {
                        _event1.WaitOne();
                    }

                    numRead++;
                    var newBlock = ReadNewBlock(br, numRead);

                    lock (queueRead)
                    {
                        if (newBlock.block.Length != 0)
                        {
                            queueRead.Enqueue(newBlock);
                        }
                        else
                        {
                            source.Close();
                        }
                    }
                }
            }
            flagReadBlocks = false;
        }

        private Structures.Block ReadNewBlock(BinaryReader source, int numReads)
        {
            Structures.Block newBlock = new Structures.Block();
            try
            {
                newBlock.block = source.ReadBytes(bufferLength);
                newBlock.idBlock = numReads;

                return newBlock;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("\nFile stopped reading.");
                newBlock.block = new byte[] { };

                return newBlock;
            }
        }

        private void WriteCompressBlock()
        {
            int i = 0;
            using (var dest = new FileStream(destinationFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            using (BinaryWriter br = new BinaryWriter(dest))
            {
                WriteOptions(br, dest.Length);

                while (dest.CanWrite)
                {
                    if (queueCompression.Count != 0)
                    {
                        var writeBlock = GetCompressBlock();

                        if (writeBlock.block.Length != 0)
                        {
                            WriteBlock(br, writeBlock);

                            _event.Set();
                            i++;
                        }
                    }

                    if (i >= countBlocks)
                    {
                        break;
                    }
                }

                if (i >= countBlocks)
                {
                    Console.WriteLine("The compression was completed successfully");
                    Console.WriteLine("D - list of commands");
                }
            }
        }

        private void WriteOptions(BinaryWriter write, long length)
        {
            write.Write(option.fileLength);
            write.Write(option.block);
        }

        private void CreateOptions(long length)
        {
            option.fileLength = length;
            option.block = bufferLength;

            countBlocks = CalculateCountBlocs(option.fileLength, option.block);
        }

        private long CalculateCountBlocs(long length, int block)
        {
            long count = 0;

            if (length % block != 0)
            {
                count = (length / block) + 1;
            }
            else
            {
                count = length / block;
            }

            return count;
        }

        private void WriteBlock(BinaryWriter write, Structures.Block block)
        {
            try
            {
                write.Write(block.idBlock);
                write.Write(block.block.Length);
                write.Write(block.block);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("\nFile write terminated.");
            }
        }

        private Structures.Block GetCompressBlock()
        {
            Structures.Block compressBlock = new Structures.Block();
            lock (queueCompression)
            {
                compressBlock = queueCompression.Dequeue();
            }
            return compressBlock;
        }

        private void CompressBlock()
        {
            Structures.Block block = new Structures.Block();

            while (flagReadBlocks || queueRead.Count != 0)
            {
                block.block = new byte[] { };

                lock (queueRead)
                {
                    if (queueCompression.Count <= arrayBlocks && queueRead.Count != 0)
                    {
                        block = queueRead.Dequeue();
                        _event1.Set();
                    }

                    if (queueRead.Count >= arrayBlocks)
                    {
                        _event.WaitOne();
                    }
                }

                if (block.block.Length != 0)
                {
                    var compBlock = Compression(block);

                    lock (queueCompression)
                    {
                        queueCompression.Enqueue(compBlock);
                    }
                }
            }
        }

        private Structures.Block Compression(Structures.Block block)
        {
            Structures.Block comprBlock = new Structures.Block();

            using (var result = new MemoryStream())
            {
                using (var gZipStream = new GZipStream(result, CompressionMode.Compress))
                {
                    gZipStream.Write(block.block, 0, block.block.Length);
                }

                comprBlock.block = result.GetBuffer();
                comprBlock.idBlock = block.idBlock;
            }

            return comprBlock;
        }
    }
}
