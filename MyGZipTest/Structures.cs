﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace MyGZipTest
{
    class Structures
    {
        public struct Option
        {
            public long fileLength;
            public int block;
        }

        public struct Block
        {
            public int idBlock;
            //public int sizeBlock;
            public byte[] block;
        }
    }
}
