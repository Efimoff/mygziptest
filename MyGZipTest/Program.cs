using System;
using System.IO;

namespace MyGZipTest
{
    class Program
    {
        private static int bufferLength = 1024 * 1024;

        static void Main(string[] args)
        {
            WriteDescription();

            while (true)
            {
                string command = Console.ReadLine().ToLower();

                switch (command)
                {
                    case "start c":
                        StartComresFile();
                        break;
                    case "start d":
                        StartDeComresFile();
                        break;
                    case "b":
                        Console.WriteLine(bufferLength);
                        break;
                    case "d":
                        WriteDescription();
                        break;
                    case "exit":
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                        break;
                }
            }
        }

        static void WriteDescription()
        {
            Console.WriteLine("Exit - to complete the program;\nStart C - start file compression;\nStart D - start the decompression of the file;\nD - list of commands;\nb - show the size of the compression block.");
        }

        static void StartComresFile()
        {
            Console.WriteLine("Enter the name of the file to compress:");
            string pathC = Console.ReadLine();
            string sourceFile = CheckPath(pathC);
            Console.WriteLine("The path to the file: {0}", sourceFile);

            Console.WriteLine("Enter the name of the resulting file:");
            string zipFile = Console.ReadLine();

            Console.WriteLine("Enter the block size:");
            bufferLength = BlockSize();
            Console.WriteLine("The block size: {0}", bufferLength);

            Console.WriteLine("Press any key to start compressing");
            Console.ReadKey();
            Console.WriteLine();

            StartWork(sourceFile, zipFile, "comp");
        }

        static void StartDeComresFile()
        {
            Console.WriteLine("Enter the name of the file to decompress:");
            string pathD = Console.ReadLine();
            string deCompFile = CheckPath(pathD);
            Console.WriteLine("The path to the file: {0}", deCompFile);

            Console.WriteLine("Enter the name of the resulting file:");
            string deFile = Console.ReadLine();

            Console.WriteLine("Press any key to start compressing");
            Console.ReadKey();
            Console.WriteLine();

            StartWork(deCompFile, deFile, "decomp");
        }

        static void StartWork(string sourceFile, string zipFile, string command)
        {
            if (sourceFile != "" && command == "comp")
            {
                ComresFile comresFile = new ComresFile(bufferLength, sourceFile, zipFile);
                comresFile.Work();
            } else if (sourceFile != "" && command == "decomp")
            {
                DeCompresingFile deCompresingFile = new DeCompresingFile(sourceFile, zipFile);
                deCompresingFile.Work();
            }
            else
            {
                Console.WriteLine("File not found.\nThe path is empty.");
            }
        }

        static string CheckPath(string path)
        {
            string p = AppDomain.CurrentDomain.BaseDirectory + path;
            Console.WriteLine(p);
            if (File.Exists(p))
            {
                return p;
            }
            Console.WriteLine("File not found.\nThe path is empty.");
            return "";
        }

        static int BlockSize()
        {
            string values = Console.ReadLine();
            try
            {
                int number = Int32.Parse(values);
                return number;
            }
            catch (FormatException)
            {
                Console.WriteLine("{0}: Bad format", values);
                Console.WriteLine("Has a default value of 1 megabyte");

                return bufferLength;
            }
            catch (OverflowException)
            {
                Console.WriteLine("{0}: Overflow", values);
                Console.WriteLine("Has a default value of 1 megabyte");

                return bufferLength;
            }
        }
    }    
}
